
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;


public class Indexer
{
    public static void main(String[] args) throws Exception {


        System.out.println("Escolha uma opção:\n1.--freq-word PALAVRA ARQUIVO\n2.freq N ARQUIVO\n3.arquivos mais relevantes por termo \n");
        DataInputStream in = new DataInputStream(System.in);
        int opcao = Integer.parseInt(in.readLine());
        Map<String, Integer> frequencia = null;
        switch (opcao) {
            case 1:

                int contagem = 0;
                String s;
                String[] buffer;
                File f1 = new File("/Users/mariamuncinelli/Downloads/1gb.txt");
                FileReader fr = new FileReader(f1);
                BufferedReader bfr = new BufferedReader(fr);
                Scanner sc = new Scanner(System.in);
                System.out.println("Escolha uma palavra para a busca");
                String palavra = sc.nextLine().toLowerCase();


                while ((s = bfr.readLine()) != null) {
                    buffer = s.split(" ");
                    for (String chr : buffer) {
                        if (chr.toLowerCase().equals(palavra)) {
                            contagem++;
                        }
                    }
                }
                if (contagem == 0) {
                    System.out.println("palavra não encontrada");
                } else {
                    System.out.println("palavra: " + palavra + " encontrada. Número de aparições: " + contagem);
                }

                fr.close();
                break;
            case 2:

                File f2 = new File("/Users/mariamuncinelli/Downloads/1gb.txt");
                FileReader frr = new FileReader(f2);
                Path path;
                BufferedReader reader = new BufferedReader(new FileReader(f2.toString()));

                frequencia = new HashMap<>();

                String line = reader.readLine();
                while (line != null) {

                    if (!line.trim().equals("")) {
                        String[] words = line.split(" ");

                        for (String word : words) {
                            if (word == null || word.trim().equals("")) {
                                continue;
                            }
                            String processed = word.toLowerCase();
                            processed = processed.replace(",", "");

                            if (frequencia.containsKey(processed)) {
                                frequencia.put(processed,
                                        frequencia.get(processed) + 1);
                            } else {
                                frequencia.put(processed, 1);
                            }
                        }
                    }

                    line = reader.readLine();
                }



                Map<String, Integer> sortedByValue = ordernarPorValor(frequencia);

                System.out.println("Palavras mais recorrentes em ordem decrescente: " + sortedByValue);
                break;
            case 3:



        }


    }





    //System.out.println(frequencia);




    private static Map<String, Integer> ordernarPorValor(Map<String, Integer> frequencia) {
        List<Map.Entry<String, Integer>> entryList = new ArrayList<>(frequencia.entrySet());
        Scanner scanner = new Scanner(System.in);
        System.out.print("Escolha a quantidade de valores:");
        int threshold = scanner.nextInt();
        // ordenar a lista
        entryList.sort(new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
                return entry2.getValue().compareTo(entry1.getValue());
            }
        });

        // Filtrar palavras com menos de 2 caracteres
        List<Map.Entry<String, Integer>> filteredList = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : entryList) {
            if (entry.getKey().length() >= 2) {
                filteredList.add(entry);
            }
        }

        // Limitar a lista por pros números escolhidos
        if (filteredList.size() > threshold) {
            filteredList = filteredList.subList(0, threshold);
        }

        // converte a lista de volta para mapa
        Map<String, Integer> resultMap = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : filteredList) {
            resultMap.put(entry.getKey(), entry.getValue());
        }

        return resultMap;


    }

}
















